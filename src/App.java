import com.devcamp.Movable;
import com.devcamp.MovablePoint;

public class App {
    public static void main(String[] args) throws Exception {
        System.out.println("Hello, World!");
        Movable point1 = new MovablePoint(0, 0);
        MovablePoint point2 = new MovablePoint(10, 8);

        point1.moveUp();
        point2.moveDown();

        System.out.println(point1);
        System.out.println(point2);

        point1.moveLeft();
        point2.moveRight();

        System.out.println(point1);
        System.out.println(point2);

    }
}
